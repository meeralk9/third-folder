﻿using LoginFunctionality.Authentication;
using LoginFunctionality.EmployeeData;
using LoginFunctionality.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginFunctionality.Repositories
{
    public class EmployeeRepository : IEmployeeeData
    {
        private AppDbContext _context;

        public EmployeeRepository(AppDbContext context)
        {
            _context = context;
        }
        public void Delete(EmployeeViewModel employee)
        {
            _context.Employees.Remove(employee);
        }

        public List<EmployeeViewModel> GetEmployee()
        {
            return _context.Employees.ToList();             
        }

        public EmployeeViewModel GetEmployee(int id)
        {
            return _context.Employees.Where(x => x.EmployeeID == id).FirstOrDefault();
        }

        public void insert(EmployeeViewModel employee)
        {
            _context.Employees.Add(employee);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(EmployeeViewModel employee)
        {
            _context.Employees.Update(employee);
        }
    }
}
