﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LoginFunctionality.Models
{
    [Table("t_Employee")]
    public class EmployeeViewModel
    {
        [Key]
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Designation { get; set; }
        public string Salary { get; set; }
        public string AllowedLeaves { get; set; }
        
    }
}
