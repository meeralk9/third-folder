﻿using LoginFunctionality.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginFunctionality.EmployeeData
{
    public interface IEmployeeeData
    {
        List<EmployeeViewModel> GetEmployee();
        EmployeeViewModel GetEmployee(int id);
        void Update(EmployeeViewModel Employee);
        void insert(EmployeeViewModel empdto);
        void Delete(EmployeeViewModel Employee);
        void Save();
    }
}
